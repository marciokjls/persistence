connect system/oracle as sysdba
alter session set "_ORACLE_SCRIPT"=true;
--**********************************
--Esquema persistence
--**********************************

create tablespace persistence datafile '/opt/oracle/oradata/persistence01.dbf' size 400M online;
create tablespace idx_persistence datafile '/opt/oracle/oradata/idx_persistence01.dbf' size 400M;
create user persistence identified by persistence default tablespace persistence temporary tablespace temp;
alter user persistence quota unlimited on persistence;
grant resource to persistence;
grant connect to persistence;
grant create view to persistence;
grant create job to persistence;
grant create procedure to persistence;
grant create materialized view to persistence;
alter user persistence default role connect, resource;

exit;
